export default {
  npmRegistryUri:
    process.env.VUE_APP_NPM_REGISTRY_URI ??
    'https://registry.npmjs.org/-/v1/search',
  // jsDelivrUri:
  //   process.env.VUE_APP_JSDELIVR_URI ??
  //   'https://data.jsdelivr.com/v1/stats/packages/npm',
  debouncers: {
    default: 500,
  },
};
