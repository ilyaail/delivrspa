import Vue from 'vue';
import Router from 'vue-router';

import HomePage from '../src/views/HomePage.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomePage,
    }
  ],
  scrollBehavior() {
    window.scrollTo(0, 0);
  },
});
