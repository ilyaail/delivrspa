import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import config from '../../config';

Vue.use(Vuex);

export const mutations = {
  SET_ERROR: 'SET_ERROR',
  SET_PACKAGES: 'SET_PACKAGES',
  SET_SEARCH_QUERY: 'SET_SEARCH_QUERY',
  SET_PAGE_AND_ITEMS_PERPAGE: 'SET_PAGE_AND_ITEMS_PERPAGE',
};
export const actions = {
  FETCH_PACKAGES_LIST: 'FETCH_PACKAGES_LIST',
  SEARCH_QUERY: 'SEARCH_QUERY',
};

export default new Vuex.Store({
  state: {
    packageList: [],
    selectedPackage: null,
    packagesPerPage: 10,
    selectedPage: 1,
    packagesTotalCount: 10,
    searchQuery: '',
    error: null,
    totalPackages: 0,
  },
  mutations: {
    SET_PACKAGES(state, { totalPackages, packageList }) {
      state.packageList = packageList;
      state.totalPackages = totalPackages;
    },
    SET_PAGE_AND_ITEMS_PERPAGE(state, { page, itemsPerPage }) {
      state.selectedPage = page;
      state.packagesPerPage = itemsPerPage;
    },
    SET_ERROR(state, payload) {
      state.error = payload;
    },
    SET_SEARCH_QUERY(state, payload) {
      state.selectedPage = 1;
      state.searchQuery = payload;
    },
  },
  actions: {
    async FETCH_PACKAGES_LIST({ commit, state }, options) {
      commit('SET_PAGE_AND_ITEMS_PERPAGE', options);
      try {
        const response = await axios.get(config.npmRegistryUri, {
          params: {
            text: state.searchQuery.length == 0 ? 'vue' : state.searchQuery,
            size: state.packagesTotalCount,
            from:
              state.selectedPage * state.packagesPerPage -
              state.packagesPerPage,
          },
        });
        const parsedResponse = response.data.objects.map((item) => {
          return item.package;
        });
        commit(mutations.SET_PACKAGES, {
          packageList: parsedResponse,
          totalPackages: response.data.total,
        });
      } catch (e) {
        commit(mutations.SET_ERROR, e);
      }
    },
    SEARCH_QUERY({ commit }, query) {
      commit(mutations.SET_SEARCH_QUERY, query);
    },
  },
});
